import { Component, OnInit, Inject, HostListener } from '@angular/core';
import { FilePreviewOverlayRef } from '../file-preview-overlay-ref';
import { FILE_PREVIEW_DIALOG_DATA } from '../file-preview-overlay.tokens';


// Keycode for ESCAPE
const ESCAPE = 27;

@Component({
  selector: 'app-file-preview-overlay',
  templateUrl: './file-preview-overlay.component.html',
  styleUrls: ['./file-preview-overlay.component.css']
})
export class FilePreviewOverlayComponent implements OnInit {

  constructor( public dialogRef: FilePreviewOverlayRef,
    @Inject(FILE_PREVIEW_DIALOG_DATA) public image: any) { }

    @HostListener('document:keydown', ['$event']) private handleKeydown(event: KeyboardEvent) {
      if (event.keyCode === ESCAPE) {
        this.dialogRef.close();
      }
    }
    
  ngOnInit() {
  }

}
