import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { OverlayModule } from "@angular/cdk/overlay";
import { FormsModule } from '@angular/forms';
import { MatToolbarModule, MatListModule, MatIconModule, MatButtonModule } from '@angular/material';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FilePreviewOverlayComponent } from './file-preview-overlay/file-preview-overlay.component';
import { FilePreviewOverlayService } from './file-preview-overlay.service';

// https://blog.thoughtram.io/angular/2017/11/20/custom-overlays-with-angulars-cdk.html

@NgModule({
  declarations: [
    AppComponent,
    FilePreviewOverlayComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    OverlayModule,
    FormsModule,
    MatToolbarModule, 
    MatListModule,
    MatIconModule,
    MatButtonModule
  ],
  providers: [FilePreviewOverlayService],
  bootstrap: [AppComponent],
  entryComponents:[FilePreviewOverlayComponent]
})
export class AppModule { }
